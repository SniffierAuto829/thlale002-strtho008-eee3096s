/*
 * BinClock.c
 * Jarrod Olivier
 * Modified by Keegan Crankshaw
 * Further Modified By: Mark Njoroge 
 *
 * 
 * STRTHO008 THLALE002
 * 18/08/21
*/

#include <signal.h> //for catching signals
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <stdio.h> //For printf functions
#include <stdlib.h> // For system functions

#include "BinClock.h"
#include "CurrentTime.h"

//Global variables
int hours, mins, secs;
long lastInterruptTime = 0; //Used for button debounce
int RTC; //Holds the RTC instance

int LED1 = 5; //defined wPi number of LED
int sw1 = 4; //defined wPi number of sw1
int sw2 = 6; //defined wPi number of sw2

int HIGH1 =1; //defined constant HIGH1
int LOW0 =0; //defined constant LOW0



int HH,MM,SS;


// Clean up function to avoid damaging used pins
void CleanUp(int sig){
	printf("Cleaning up\n");

	//Set LED to low then input mode
	//Logic here
	
	digitalWrite (LED1,LOW0) ; //set GPIO output to LED1 as LOW0
	pinMode (LED1,INPUT) ; //set GPIO pin connected to LED1 as input
	
	
	for (int j=0; j < sizeof(BTNS)/sizeof(BTNS[0]); j++) {
		pinMode(BTNS[j],INPUT);
	}

	exit(0);

}

void initGPIO(void){
	/* 
	 * Sets GPIO using wiringPi pins. see pinout.xyz for specific wiringPi pins
	 * You can also use "gpio readall" in the command line to get the pins
	 * Note: wiringPi does not use GPIO or board pin numbers (unless specifically set to that mode)
	 */
	printf("Setting up\n");
	wiringPiSetup(); //This is the default mode. If you want to change pinouts, be aware
	

	RTC = wiringPiI2CSetup(RTCAddr); //Set up the RTC
	
	pinMode (LED1,OUTPUT); //set GPIO pin to LED1 as output
	//Set up the LED
	//Write your Logic here

	
	printf("LED and RTC done\n");
	
	//Set up the Buttons
	for(int j=0; j < sizeof(BTNS)/sizeof(BTNS[0]); j++){
		pinMode(BTNS[j], INPUT);
		pullUpDnControl(BTNS[j], PUD_UP);
	}
	
	//Attach interrupts to Buttons
	//Write your logic here
	pinMode(sw1,INPUT); //set GPIO pin to sw1 as INPUT
	pullUpDnControl(sw1,PUD_UP); //configured GPIO pin to sw1 as pull ip resistor
	wiringPiISR(sw1,INT_EDGE_RISING,&hourInc); //configure sw1 to rising edge and use function hourInc
	
	pinMode(sw2,INPUT); //set GPIO pin to sw2 as INPUT
	pullUpDnControl(sw2, PUD_UP); //configured GPIO pin to sw2 as pull ip resistor
	wiringPiISR(sw2,INT_EDGE_RISING,&minInc); //configure sw2 to rising edge and use function minInc


	printf("BTNS done\n");
	printf("Setup done\n");
}


/*
 * The main function
 * This function is called, and calls all relevant functions we've written
 */
int main(void){
	signal(SIGINT,CleanUp);
	initGPIO();

//	Set random time (3:04PM)
	//You can comment this file out later
	wiringPiI2CWriteReg8(RTC, HOUR_REGISTER, 0x13+TIMEZONE);
	wiringPiI2CWriteReg8(RTC, MIN_REGISTER, 0x4);
	wiringPiI2CWriteReg8(RTC, SEC_REGISTER, 0x00);
	
	// Repeat this until we shut down
	for (;;){
		//Fetch the time from the RTC
		//Write your logic here 

		hours = wiringPiI2CReadReg8(RTC,HOUR_REGISTER); //reads hour time from RTC using I2C protocol
		mins = wiringPiI2CReadReg8(RTC,MIN_REGISTER);	//reads minutes time from RTC using I2C protocol
		secs = wiringPiI2CReadReg8(RTC,SEC_REGISTER);  //reads secs time from RTC using I2C protocol
		

		if (secs>=60) {secs=wiringPiI2CWriteReg8(RTC, SEC_REGISTER, 0);
				mins+=1; wiringPiI2CWriteReg8(RTC, MIN_REGISTER, mins);
				//resets secs to 0 and increments minutes by 1 when secs>60
		}
		if (mins>=60) {mins=wiringPiI2CWriteReg8(RTC, MIN_REGISTER, 0);
				hours+=1; wiringPiI2CWriteReg8(RTC, MIN_REGISTER, hours);
				//resets mins to 0 and increments hours by 1 when secs>60
		}
		if (hours>=24) {hours=wiringPiI2CWriteReg8(RTC, HOUR_REGISTER, 0);}
			//resets hours to 0 when hours > 24
		//Toggle Seconds LED
		//Write your logic here
		
		
		digitalWrite(LED1,LOW0); //set GPIO to LED1 as LOW0 turns LED1 on
		printf("The LED is ON\n"); 
		delay(1000);//second delay
			
		digitalWrite(LED1,HIGH1); //set GPIO to LED1 as HIGH1 turns LED1 off
		printf("The LED is OFF\n");
		delay(1000);//second delay
		
		
		// Print out the time we have stored on our RTC
		printf("The current time is: %d:%d:%d\n", hours, mins, secs);
	}
	return 0;
}

/*
 * Changes the hour format to 12 hours
 */
int hFormat(int hours){
	/*formats to 12h*/
	if (hours >= 24){
		hours = 0;
	}
	else if (hours > 12){
		hours -= 12;
	}
	return (int)hours;
}


/*
 * hexCompensation
 * This function may not be necessary if you use bit-shifting rather than decimal checking for writing out time values
 * Convert HEX or BCD value to DEC where 0x45 == 0d45 	
 */
int hexCompensation(int units){

	int unitsU = units%0x10;

	if (units >= 0x50){
		units = 50 + unitsU;
	}
	else if (units >= 0x40){
		units = 40 + unitsU;
	}
	else if (units >= 0x30){
		units = 30 + unitsU;
	}
	else if (units >= 0x20){
		units = 20 + unitsU;
	}
	else if (units >= 0x10){
		units = 10 + unitsU;
	}
	return units;
}


/*
 * decCompensation
 * This function "undoes" hexCompensation in order to write the correct base 16 value through I2C
 */
int decCompensation(int units){
	int unitsU = units%10;

	if (units >= 50){
		units = 0x50 + unitsU;
	}
	else if (units >= 40){
		units = 0x40 + unitsU;
	}
	else if (units >= 30){
		units = 0x30 + unitsU;
	}
	else if (units >= 20){
		units = 0x20 + unitsU;
	}
	else if (units >= 10){
		units = 0x10 + unitsU;
	}
	return units;
}


/*
 * hourInc
 * Fetch the hour value off the RTC, increase it by 1, and write back
 * Be sure to cater for there only being 23 hours in a day
 * Software Debouncing should be used
 */
void hourInc(void){
	//Debounce
	long interruptTime = millis();

	if (interruptTime - lastInterruptTime>200){
		printf("Interrupt 1 triggered, %x\n", hours);
		if (hours==23){wiringPiI2CWriteReg8(RTC, HOUR_REGISTER, 0);}
		else {

		hours = wiringPiI2CReadReg8(RTC,HOUR_REGISTER); //reads hour time from RTC
		hours = hours +1;//increments hour by 1
		wiringPiI2CWriteReg8(RTC, HOUR_REGISTER, hours); //writes incremented hour to RTC
		}
		
		//Fetch RTC Time
		//Increase hours by 1, ensuring not to overflow
		//Write hours back to the RTC
	}
	lastInterruptTime = interruptTime;
}

/* 
 * minInc
 * Fetch the minute value off the RTC, increase it by 1, and write back
 * Be sure to cater for there only being 60 minutes in an hour
 * Software Debouncing should be used
 */
void minInc(void){
	long interruptTime = millis();

	if (interruptTime - lastInterruptTime>200){
		printf("Interrupt 2 triggered, %x\n", mins);
		
		int minutes = wiringPiI2CReadReg8(RTC,MIN_REGISTER); //reads minute time from RTC
		minutes = minutes +1; //increments minutes by 1
		if(minutes >= 59){ //resets minutes to 0 if they exceed 59
			minutes = 0;
			wiringPiI2CWriteReg8(RTC, MIN_REGISTER, minutes);//writes minutes to RTC
		}else{
			wiringPiI2CWriteReg8(RTC, MIN_REGISTER, minutes); //writes incremented minute time to RTC
		}
		//Fetch RTC Time
		//Increase minutes by 1, ensuring not to overflow
		//Write minutes back to the RTC
	}
	lastInterruptTime = interruptTime;
}

//This interrupt will fetch current time from another script and write it to the clock registers
//This functions will toggle a flag that is checked in main
void toggleTime(void){
	long interruptTime = millis();

	if (interruptTime - lastInterruptTime>200){
		HH = getHours();
		MM = getMins();
		SS = getSecs();

		HH = hFormat(HH);
		HH = decCompensation(HH);
		wiringPiI2CWriteReg8(RTC, HOUR_REGISTER, HH);

		MM = decCompensation(MM);
		wiringPiI2CWriteReg8(RTC, MIN_REGISTER, MM);

		SS = decCompensation(SS);
		wiringPiI2CWriteReg8(RTC, SEC_REGISTER, 0b10000000+SS);

	}
	lastInterruptTime = interruptTime;
}
