import datetime
import threading
import time
import RPi.GPIO as GPIO
import adafruit_mcp3xxx.mcp3008 as MCP
import board
import busio
import digitalio
from adafruit_mcp3xxx.analog_in import AnalogIn

global start_time
start_time=datetime.datetime.now()
#get current time and save in global variable, used to calculate run time
global interval
interval = 5
#sets the current interval between samples taken by the light and temperature snesors

def setup():
    # create the spi bus
    spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
    # create the cs (chip select)
    cs = digitalio.DigitalInOut(board.D5)
    # create the mcp object
    mcp = MCP.MCP3008(spi, cs)
    #set up the button using digitalio
    button = digitalio.DigitalInOut(board.D24)
    button.switch_to_input(pull=digitalio.Pull.UP)

def change_interval():
    """
        changes the sampling interval, called when button pressed
    """
    global interval
    if interval == 5:
        interval =10
        print("button pressed interval = 10")
        time.sleep(10)
    elif interval ==10:
        interval = 1
        print("button pressed interval = 1")
        time.sleep(5)
    elif interval == 1:
        interval = 5
        print("button pressed interval = 5")
        time.sleep(1)

def getLDR():
    """
        Get voltage reading from ADC
    """
    # create the spi bus
    spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
    # create the cs (chip select)
    cs = digitalio.DigitalInOut(board.D5)
    # create the mcp object
    mcp = MCP.MCP3008(spi, cs)
    # create an analog input channel on pin 2
    chan = AnalogIn(mcp, MCP.P2)
    return chan.value
    #print("Raw ADC Value: ", chan.value)
    #print("ADC Voltage: " + str(chan.voltage) + "V")

def getTemp_voltage():
    """
        Get voltage reading from ADC
    """
    # create the spi bus
    spi = busio.SPI(clock=board.SCK, MISO=board.MISO, MOSI=board.MOSI)
    # create the cs (chip select)
    cs = digitalio.DigitalInOut(board.D5)
    # create the mcp object
    mcp = MCP.MCP3008(spi, cs)
    # create an analog input channel on pin 1
    chan_Temp = AnalogIn(mcp, MCP.P1)
    temp_voltage = chan_Temp.voltage
    return temp_voltage

def convertDegrees(temp_voltage):
    """
        This function converts voltage to temperature using a formula found in the MCP9700 datasheet
        Constants 0.01 and 0.5 are found in the datasheet and refer to the Temperature coeficcient and
        sensor's outut voltage at 0C
    """
    #uses formula from MCP9700 datasheet to convert voltage reading to temperature
    #constants 0.01 and 0.5 read from datasheet
    temp = (temp_voltage - 0.5)
    temp = temp / 0.01
    return temp

def format_output():
    """
        This function gets output from temperature and light functions and adds them to string in the required format
    """
    #gets output from temperature and light functions and adds them to string in the required format
    temp_voltage = getTemp_voltage() #uses temp_voltage() to get voltage output of temp sensor from ADC
    temp_deg = round(convertDegrees(temp_voltage), 1)
    #uses the convertDegrees function and rounds the output to one decimal point, rounding to help adhere to format guidelines
    temp_deg = str(temp_deg)
    #converts to string which can then be printed
    temp_voltage = round(temp_voltage, 3)
    #round temp_voltage to 3 decimal places then converts to string to be printed
    temp_voltage = str(temp_voltage)
    light = round(getLDR(),5)
    light = str(light)
    #round output of LDR to 5 decimal places and convert to string for printing
    runTime = round((datetime.datetime.now()-start_time).total_seconds())
    #difference between time when program originally started running and current time
    #time difference of seconds used to adhere to formatting
    runTime = str(runTime)
    #convert to string to be printed
    print(runTime +"s \t\t " + temp_voltage +" \t\t " + temp_deg +" C\t\t " + light)


def print_time_thread():
    """
    This function prints the time to the screen every few seconds depending on the value of the global variable interval
    this variable is controlled by the button connected to GPIO pin 24
    """
    thread = threading.Timer(interval, print_time_thread)
    thread.daemon = True  # Daemon threads exit when the program does
    thread.start()
    format_output()

if __name__ == "__main__":
    print("Runtime \t Temp Reading \t Temp \t\t Light Reading")
    print_time_thread()  # call it once to start the thread
    # Tell our program to run indefinitely
    setup()
    button = digitalio.DigitalInOut(board.D24)
    #set up button on GPIO pin 24 with pull up resistor
    button.switch_to_input(pull=digitalio.Pull.UP)
    while True:
        if not button.value: #waits for button state to change
            x = threading.Thread(target=change_interval) 
            x.start()
            x.join()
        pass
        

