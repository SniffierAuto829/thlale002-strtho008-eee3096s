import datetime
import threading
import time

import RPi.GPIO as GPIO
import adafruit_mcp3xxx.mcp3008 as MCP
import board
import busio
import digitalio
from adafruit_mcp3xxx.analog_in import AnalogIn

global interval
interval = 5

def setup():
    #set up the button using digitalio
    button = digitalio.DigitalInOut(board.D24)
    button.switch_to_input(pull=digitalio.Pull.UP)

def change_interval():
    global interval
    
    if interval ==5:
        interval =10
        print("button pressed interval = 10")
        time.sleep(10)

    elif interval ==10:
        interval = 1
        print("button pressed interval = 1")
        time.sleep(5)

    elif interval == 1:
        interval = 5
        print("button pressed interval = 5")
        time.sleep(10)    
  

if __name__ == "__main__":
    print("Runtime \t Temp Reading \t Temp \t\t Light Reading")
    
    setup()
    button = digitalio.DigitalInOut(board.D24)
    button.switch_to_input(pull=digitalio.Pull.UP)
    
    while True:
        if not button.value:
            x = threading.Thread(target=change_interval) 
            x.start()
            x.join()
        pass




